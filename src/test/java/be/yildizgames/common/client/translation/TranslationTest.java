/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.common.client.translation;

import be.yildizgames.common.util.language.LanguageValue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Grégory Van den Borre
 */
final class TranslationTest {

    @Test
    void testAddLanguageNullArg() {
        assertThrows(AssertionError.class, () -> Translation.getInstance().addLanguage(null, new LanguageProvider()));
    }

    @Test
    void testAddLanguageArgNull() {
        assertThrows(AssertionError.class, () -> Translation.getInstance().addLanguage(LanguageValue.EN, null));
    }

    @Test
    void testAddLanguageArgArg() {
        Translation.getInstance().addLanguage(LanguageValue.EN, new LanguageProvider());
    }

    @Test
    void testChooseLanguageNotExisting() {
        Translation.getInstance().addLanguage(LanguageValue.EN, new LanguageProvider());
        assertThrows(IllegalArgumentException.class, () -> Translation.getInstance().chooseLanguage(LanguageValue.FR));
        assertThrows(AssertionError.class, () -> Translation.getInstance().chooseLanguage(null));
    }

    @Test
    void testGet() {
        LanguageProvider p = new LanguageProvider();
        p.add("test", "test-fr", "test-en");
        Translation.getInstance().addLanguage(LanguageValue.EN, p);
        Translation.getInstance().chooseLanguage(LanguageValue.EN);
        assertEquals("test-en", Translation.getInstance().translate(TranslationKey.get("test")));
    }

    @Test
    void testGetNotExisting() {
        LanguageProvider p = new LanguageProvider();
        p.add("test", "test-fr", "test-en");
        Translation.getInstance().addLanguage(LanguageValue.EN, p);
        Translation.getInstance().chooseLanguage(LanguageValue.EN);
        assertThrows(IllegalArgumentException.class, () -> Translation.getInstance().translate(TranslationKey.get("test:)")));
    }
}
